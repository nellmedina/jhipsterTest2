import { NgModule } from '@angular/core';

import { JhipsterTest2SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [JhipsterTest2SharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [JhipsterTest2SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class JhipsterTest2SharedCommonModule {}
